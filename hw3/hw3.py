import numpy as np
import random
import keras
from keras.datasets import cifar100
from keras.datasets import cifar10
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D,Activation, Flatten, Dense, Dropout
from keras.optimizers import SGD
from keras.preprocessing.image import ImageDataGenerator
from keras import backend as K
import time
from keras import backend as K
from PIL import Image 
from scipy.misc import imsave
#Load data
(photo_train, label_train), (photo_test, label_test) = cifar100.load_data()
(photo_train10, label_train10), (photo_test10, label_test10) = cifar10.load_data()

#save acc and loss
class History(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.loss = []
        self.val_loss = []
        self.acc = []
        self.val_acc = []
    def on_batch_end(self, batch, logs={}):
        self.loss.append(logs.get('loss'))    
        self.val_loss.append(logs.get('val_loss'))    
        self.acc.append(logs.get('acc'))    
        self.val_acc.append(logs.get('val_acc'))    

#preprocess input data (don't use, IDG is bad)
photo_train = photo_train.astype('float32')
photo_test = photo_test.astype('float32')
gcn = ImageDataGenerator(width_shift_range=0.5, height_shift_range=0.5,horizontal_flip=True, vertical_flip=True)
gcn.fit(photo_train)
gcn_val = ImageDataGenerator(samplewise_std_normalization=True,samplewise_center=True)
gcn_val.fit(photo_test)

#manually preprocess input data
photo_train = photo_train.astype('float32')
photo_test = photo_test.astype('float32')
def gcn(photo_train):
    for j in range(3):
        mean = np.mean(photo_train[:,:,:,j])
        std = np.std(photo_train[:,:,:,j])
        photo_train[:,:,:,j] = (photo_train[:,:,:,j]-mean)/std
    return photo_train
photo_train = gcn(photo_train)
photo_test = gcn(photo_test)

#manually flip photos
def flip_photo(photo_train,label_train, number): 
    count = 0
    while count < number:
        i = random.randint(0,50000)
        aug_photo = np.fliplr(photo_train[i,:,:,:]).copy()
        photo_train = np.concatenate((photo_train, [aug_photo]),axis=0)
        count += 1
        label = label_train[i].copy()
        label_train = np.concatenate((label_train,[label]),axis=0) 
    return photo_train,label_train

#manually crop photos
def crop_photo(photo_train, label_train, number):
    count = 0
    while count < number: 
        i = random.randint(0,50000)
        copy = photo_train[i,:,:,:].copy()
        w = random.randint(0,5)
        h = random.randint(0,5)
        for i in range(32):
            for j in range(32):
                if (i < w or i > w+25) and (j<h or j>h+25):
                    copy[i,j] = 0
        photo_train = np.concatenate((photo_train, [copy]),axis=0)
        label = label_train[i].copy()
        label_train = np.concatenate((label_train,[label]),axis=0)
        count += 1
    return (photo_train, label_train)

#change number of images per category 
def limit_input(photo_train, label_train, number):
    index = np.zeros(100)
    new_photo_train = []
    new_label_train = []
    for i in range(len(photo_train)):
        if all(a >= number for a in index):
            break
        else: 
            num = label_train[i]
            if index[num] < number: 
                new_photo_train.append(photo_train[i])
                new_label_train.append(label_train[i])
                index[num] += 1
    return np.array(new_photo_train), np.array(new_label_train)
photo_train, label_train = limit_input(photo_train, label_train, 300)

#One hot encoding
label_train = np_utils.to_categorical(label_train)
label_test = np_utils.to_categorical(label_test)
label_train10 = np_utils.to_categorical(label_train10)
label_test10 = np_utils.to_categorical(label_test10)

#Setting up the model
model = Sequential()
model.add(Convolution2D(32,3,3, input_shape = (photo_train[0].shape), border_mode='same', activation='relu'))
model.add(Convolution2D(32,3,3,activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))
model.add(Convolution2D(64,3,3,activation='relu'))
model.add(Convolution2D(64,3,3,activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(1000,activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(len(label_train[0]),activation='softmax'))
print(model.summary())

#testing parameters and network depth
model = Sequential()
model.add(Convolution2D(32,3,3, input_shape = (photo_train[0].shape), border_mode='same', activation='relu'))
model.add(Convolution2D(32,3,3,activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))
model.add(Convolution2D(64,3,3,activation='relu'))
model.add(Convolution2D(64,3,3,activation='relu'))
model.add(Convolution2D(64,3,3,activation='relu'))
model.add(Convolution2D(64,3,3,activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(2400,activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(len(label_train[0]),activation='softmax'))
print(model.summary())

#fine-tuning
top_model = Sequential() 
top_model.add(Flatten(input_shape=model.output_shape[1:]))
top_model.add(Dense(100,activation='relu'))
top_model.add(Dropout(0.5))
top_model.add(Dense(10, activation='softmax'))
for i in range(4):
    model.pop()
for layer in model.layers[:25]:
    layer.trainable = False
mode.add(top_model)

#train netwrok
epochs = 25
alpha = 0.01
batch = 50
gradient = keras.optimizers.RMSprop(lr=0.0001, rho=0.9, epsilon=1e-08, decay=0.0)
model.compile(loss='categorical_crossentropy',optimizer= gradient, metrics=['accuracy'])

#fit model using dataimagegenerator (not using anymore)
model.fit_generator(gcn.flow(photo_train,label_train, batch_size = batch), nb_epoch=epochs, validation_data=(photo_test,label_test),nb_val_samples= len(label_test),samples_per_epoch=len(photo_train))

#fit model
history = History()
model.fit(photo_train, label_train, validation_data=(photo_test, label_test), nb_epoch=epochs, shuffle=True, batch_size = batch, callbacks=[history])

#fit model for fine-tuning
model.fit(photo_train10, label_train10, validation_data=(photo_test10, label_test10), nb_epoch=epochs, shuffle=True, batch_size = batch)

#saving model and weight
model_save = model.to_json()
with open("model.json", "w") as json_file:
        json_file.write(model_save)
model.save_weights("model.h5")
model.load_weights("../model.h5")

#trying to save plot (didnt work, can't import package)
'''
fig = plt.figure()
ax1 = fig.add_subplot(211)
ax1.plot(history.acc)
ax1.plot(history.val_acc)
ax1.ylabel('% accuracy')
ax1.xlabel('rounds')
ax1.legend(['train_accuracy', 'val_accuracy'], loc='lower left')
ax2 = fig.add_subplot(212)
ax2.plot(history.loss)
ax2.plot(history.val_loss)
ax2.ylabel('loss')
ax2.xlabel('rounds')
ax2.legend(['train_loss', 'val_loss'], loc='lower left')
fig.savefig('basic_network.png')
'''

##retriving fliter image functions
def normalize(x):
    return x / (K.sqrt(K.mean(K.square(x))) + 1e-5)
def deprocess_image(x):
    x = ((x-x.mean())/(x.std() +1e-5)) *0.1 + 0.5
    x = np.clip(x, 0, 1)
    x *= 255
    #if K.image_dim_ordering() == 'th':
    #    x = x.transpose((1, 2, 0))
    x = np.clip(x, 0, 255).astype('uint8')
    return x
def filter_images(layer_output, filters):
    w,h = (32,32)
    input_image = model.input
    save_filters = []
    for filter_index in range(0,filters):
        loss = K.mean(layer_output[:, :, :, filter_index])
        gradient = K.gradients(loss, input_image)[0]
        gradient = normalize(gradient)
        iterate = K.function([input_image], [loss, gradient])
        input_image_data = np.random.random((1, w, h, 3))
        input_image_data = (input_image_data - 0.5) * 20 + 128
        for i in range(20):
            loss_value, grads_value = iterate([input_image_data])
            input_image_data += grads_value
        image = deprocess_image(input_image_data[0])
        save_filters.append((image, loss_value))
    return save_filters

#Getting the filters
layer_dict = dict([(layer.name, layer) for layer in model.layers])
layer_output = layer_dict["convolution2d_1"].output
save_filters = filter_images(layer_output, 32)

#save_image
margin =5
width = 8*w+(8-1)*margin
height = 4*h+(4-1)*margin
stitched_filters = np.zeros((width, height, 3))
for i in range(8):
    for j in range(4):
        image, loss = save_filters[i*4+j]
        stitched_filters[(32 + margin) * i: (32 + margin) * i + 32,(32 + margin) * j: (32 + margin) * j + 32, :] = image
imsave('filters10.png', stitched_filters)
